package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsRolePermissionMapper;
import com.micro.fast.upms.pojo.UpmsRolePermission;
import com.micro.fast.upms.service.UpmsRolePermissionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsRolePermissionServiceImpl  extends SsmServiceImpl<UpmsRolePermission,Integer,UpmsRolePermissionMapper>
implements UpmsRolePermissionService<UpmsRolePermission,Integer>,InitializingBean {

  @Autowired
  private UpmsRolePermissionMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
