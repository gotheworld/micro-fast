package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsOrganizationMapper;
import com.micro.fast.upms.pojo.UpmsOrganization;
import com.micro.fast.upms.service.UpmsOrganizationService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsOrganizationServiceImpl  extends SsmServiceImpl<UpmsOrganization,Integer,UpmsOrganizationMapper>
implements UpmsOrganizationService<UpmsOrganization,Integer>,InitializingBean {

  @Autowired
  private UpmsOrganizationMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
