package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsUserOrganization;

public interface UpmsUserOrganizationMapper extends SsmMapper<UpmsUserOrganization,Integer> {
    @Override
    int deleteByPrimaryKey(Integer userOrganizationId);

    @Override
    int insert(UpmsUserOrganization record);

    @Override
    int insertSelective(UpmsUserOrganization record);

    @Override
    UpmsUserOrganization selectByPrimaryKey(Integer userOrganizationId);

    @Override
    int updateByPrimaryKeySelective(UpmsUserOrganization record);

    @Override
    int updateByPrimaryKey(UpmsUserOrganization record);
}